// go mod init server2_go

package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Products struct {
	Sku          string
	Product_name string
	Stocks       int
}

type ResponseProduct struct {
	Status  int
	Message string
	Data    []Products
}

func getProduct(w http.ResponseWriter, r *http.Request) {
	var products Products       // variable untuk memetakan data product yang terbagi menjadi 3 field
	var arr_products []Products // menampung variable products ke dalam bentuk slice
	// var responseProd ResponseProduct // variable untuk menampung data arr_produts yang nantinya digunakan

	db, err := sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/go_coba")
	defer db.Close()

	if err != nil {
		log.Fatal(err)
	}

	rows, err := db.Query("Select sku,product_name,stocks form products ORDER BY sku DESC")
	if err != nil {
		log.Print(err)
	}

	// bentuk peruangan untuk me-render data dari mySQL ke struct dan slice data products
	count := 0
	for rows.Next() {
		if err := rows.Scan(&products.Sku, &products.Product_name, &products.Stocks); err != nil {
			log.Fatal(err.Error())

		} else {
			arr_products = append(arr_products, products)
			fmt.Println(arr_products[count]) // untuk langsung menghitung index
		}
		count++
	}

	// mengubah data struct menjadi JSON
	json.NewEncoder(w).Encode(arr_products) // arr_products => responseProd

}

/*
	responseProd.Status = 1 // mengisi value = 1, dengan asumsi PASTI success
	responseProd.Message = "Success"
	responseProd.Data = arr_products // mengisi komponen Data dengan slice arr_products
*/

// json.NewEncoder(w).Encode(responseProd)

func main() {
	router := mux.NewRouter()

	// menjalurkan url
	router.HandleFunc("/getProducts", getProduct).Methods("GET") //? getProduct => returnAllProducts
	http.Handle("/", router)
	log.Fatal(http.ListenAndServe(":5432", router))
}
