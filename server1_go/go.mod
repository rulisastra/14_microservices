module server1_go

go 1.15

require (
	github.com/basgys/goxml2json v1.1.0
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.0
	golang.org/x/net v0.0.0-20201010224723-4f7140c49acb // indirect
)
