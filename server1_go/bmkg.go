package main

// localhost:4321     = bmkg
// jadi yey!

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	xj "github.com/basgys/goxml2json"
)

// 1. get XML
func getXML(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", fmt.Errorf("GET error: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("Status error: %v", resp.StatusCode)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("Read body: %v", err)
	}

	return string(data), nil
}

// 2. get Content
func getContent(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("GET error: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Status error: %v", resp.StatusCode)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("Read body: %v", err)
	}

	return data, nil
}

// bmkg xml => struct
type Infogempa struct {
	XMLName xml.Name `xml:"Infogempa"`
	Text    string   `xml:",chardata"`
	Gempa   struct {
		Text    string `xml:",chardata"`
		Tanggal string `xml:"Tanggal"`
		Jam     string `xml:"Jam"`
		Point   struct {
			Text        string `xml:",chardata"`
			Coordinates string `xml:"coordinates"`
		} `xml:"point"`
		Lintang   string `xml:"Lintang"`
		Bujur     string `xml:"Bujur"`
		Magnitude string `xml:"Magnitude"`
		Kedalaman string `xml:"Kedalaman"`
		Symbol    string `xml:"_symbol"`
		Wilayah1  string `xml:"Wilayah1"`
		Wilayah2  string `xml:"Wilayah2"`
		Wilayah3  string `xml:"Wilayah3"`
		Wilayah4  string `xml:"Wilayah4"`
		Wilayah5  string `xml:"Wilayah5"`
		Potensi   string `xml:"Potensi"`
	} `xml:"gempa"`
}

// ambil data BMKG (ver 1) =================================== main.go
/* func serverBMKG(c *gin.Context) {

	// url := https://data.bmkg.go.id/autogempa.xml

	var td Todo
	if err := c.ShouldBindJSON(&td); err != nil {
		c.JSON(http.StatusUnprocessableEntity, "invalid json")
		return
	}
	//Extract the access token metadata
	metadata, err := ExtractTokenMetadata(c.Request)
	if err != nil {
		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}
	userid, err := FetchAuth(metadata)
	if err != nil {
		c.JSON(http.StatusUnauthorized, err.Error())
		return
	}

	fmt.Println(userid)
	// td.UserID = userid // ngga guna
	url := "https://localhost:4321/bmkg"

	spaceClient := http.Client{
		Timeout: time.Second * 2, // timeorut after 2 secs
	}

	// request
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}

	// respond
	res, getErr := spaceClient.Do(req)
	if getErr != nil {
		log.Fatal(err)
	}

	if res.Body != nil {
		defer res.Body.Close()
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	var info Infogempa
	json.Unmarshal(body, &info)
	c.JSON(http.StatusCreated, info)
	fmt.Println(info)
} */

// ambil data BMKG (ver 2)  =================================== main.go
/* func DataBMKG(c *gin.Context) {
	var td Todo
	if err := c.ShouldBindJSON(&td); err != nil {
	c.JSON(http.StatusUnprocessableEntity, "invalid json")
	return
	}
	//Extract the access token metadata
	metadata, err := ExtractTokenMetadata(c.Request)
	if err != nil {
	c.JSON(http.StatusUnauthorized, "unauthorized")
	return
	}
	userid, err := FetchAuth(metadata)
	if err != nil {
	c.JSON(http.StatusUnauthorized, err.Error())
	return
	}
	td.UserID = userid
	url := "http://localhost:4321/"

	spaceClient := http.Client{
	Timeout: time.Second * 2, // Timeout after 2 seconds
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
	log.Fatal(err)
	}

	req.Header.Set("User-Agent", "spacecount-tutorial")

	res, getErr := spaceClient.Do(req)
	if getErr != nil {
	log.Fatal(getErr)
	}

	if res.Body != nil {
	defer res.Body.Close()
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
	log.Fatal(readErr)
	}

	infogempa := Infogempa{}
	jsonErr := json.Unmarshal(body, &infogempa)
	if jsonErr != nil {
	log.Fatal(jsonErr)
	}

	// fmt.Println(infogempa.Infogempa)
	c.JSON(http.StatusCreated, infogempa)
}
*/

// ============================================== ambil data == bmkg.go ==============

// data BMKG (ver 1)
// parsing data mySQL ke JSON ==== bener kok di bmkg.go

/* func returnBMKG(w http.ResponseWriter, r *http.Request) {
	var result Infogempa
	if xmlBytes, err := getXML("https://data.bmkg.go.id/autogempa.xml"); err != nil {
		log.Printf("Failed to get XML: %v", err)
	} else {
		xml.Unmarshal(xmlBytes, &result) // error di xmlBytes
	}
	fmt.Print(result)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(result)
} */

// data BMKG (ver 2)
func dataBMKG(w http.ResponseWriter, r *http.Request) {
	// xml is an io.Reader
	w.Header().Set("Content-Type", "application/json")

	if data, err := getContent("https://data.bmkg.go.id/autogempa.xml"); err != nil {
		log.Printf("Failed to get XML: %v", err)
	} else {
		// log.Println("Received XML:")
		xml := string(data)
		xml2 := strings.NewReader(xml)
		result, err := xj.Convert(xml2)
		if err != nil {
			panic("That's embarrassing...")
		}
		ra := result.String()
		fmt.Println(result.String())
		// w.Write(result)
		w.Write([]byte(ra))
		// result := json.String()
		// json.NewEncoder(w).Endcode(result)
		// w.Write(result)
	}
	// w.Write([]byte());
}

func main() {
	http.HandleFunc("/", dataBMKG)
	http.ListenAndServe(":4321", nil)
}
