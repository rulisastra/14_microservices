module gate_go

go 1.15

require (
	github.com/basgys/goxml2json v1.1.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/go-redis/redis/v7 v7.4.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.0
	github.com/twinj/uuid v1.0.0
	golang.org/x/net v0.0.0-20201010224723-4f7140c49acb // indirect
)
